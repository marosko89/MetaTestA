module MetaTestA

# package code goes here


"""
    maeBase(residuals)

Compute sum of residuals and their count from which Mean Absolute Error (MAE) is computed.
"""
function maeBase(residuals)
  sum(abs, residuals), length(residuals)
end


end # module
